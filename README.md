# readme for Smart XML Analyzer

# how to run

1. program has arguments input names :

  - for origin path  attribute name is - originPath
  
  - for origin id attribute name is - originId
  
  - for sample path attribute name is - samplePath

# Example 
java -jar agile-engine-tt-0.0.1.jar originPath=./samples/sample-0-origin.html originId=make-everything-ok-button samplePath=./samples/sample-1-evil-gemini.html

*  explanations -all attributes name must be entered via '=' sign
# After execution program you can see result 
#Result example
<code>

[INFO] - Searched element css path: [#page-wrapper > div.row:nth-child(3) > div.col-lg-8 > div.panel.panel-default > div.panel-body > a.btn.btn-warning] - css select for search element

[INFO] - ----Show diff----

[INFO] - origin attribute=id ---attribute which present in origin element
 
 old value was=make-everything-ok-button
  
 new value=

[INFO] - origin attribute=title
 
 old value was=Make-Button
  
 new value=

[INFO] - origin attribute=class

 old value was=btn btn-success
  
 new value=btn btn-warning

[INFO] - origin attribute=value

 old value was= Make everything OK
   
 new value= Make something somehow 

[INFO] - new attributes=onclick --- attributes that appeared 

 old value was= 
 
 new value=javascript:window.close(); return false;

<code>
 
 #How to makes decision about element
 - it's considered that element situated on the same place regarding to parent element and have same tag name
