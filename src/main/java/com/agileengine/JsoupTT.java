package com.agileengine;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class JsoupTT {

    private static Logger LOGGER = LoggerFactory.getLogger(JsoupTT.class);

    private static String CHARSET_NAME = "utf8";
    private static final String ORIGIN_PATH = "originPath";
    private static final String ORIGIN_ID = "originId";
    private static final String SAMPLE_PATH = "samplePath";

    public static void main(String[] args) {

//        get origin path arg
        String originPath = findArgument(ORIGIN_PATH, args);
        //        get id arg
        String targetElementId = findArgument(ORIGIN_ID, args);
//        get sample path
        String samplePath = findArgument(SAMPLE_PATH, args);

        Element originElement = findElementById(new File(originPath), targetElementId);

        Document currentDocument = getDocument(new File(samplePath));

//        get css selector
        String cssParent = originElement.parent().cssSelector();

        Optional<Element> sampleElement = getElementOnSamePlaceWithSameTag(currentDocument.select(cssParent), originElement.tagName());
        if (sampleElement.isPresent()) {
            LOGGER.info("Searched element css path: [{}]", sampleElement.get().cssSelector());
            showDiff(originElement, sampleElement.get());
        } else {
            LOGGER.info("Element not found in searched file ");
            LOGGER.info("Searched parent css path: [{}]", currentDocument.select(cssParent).first().cssSelector());
        }


    }


    private static Optional<Element> getElementOnSamePlaceWithSameTag(Elements elements, String tagName) {
        if (Objects.nonNull(elements)) {
            Optional<Element> element = elements.stream().findFirst();
            if (element.isPresent()) {
                if (element.get().tagName().equalsIgnoreCase(tagName))
                    return element;
                else return getElementOnSamePlaceWithSameTag(element.get().children(), tagName);
            }
        }
        return Optional.empty();
    }

    private static Document getDocument(File htmlFile) {
        try {
            return Jsoup.parse(
                    htmlFile,
                    CHARSET_NAME,
                    htmlFile.getAbsolutePath());
        } catch (IOException e) {
            LOGGER.warn("Error reading [{}] file ", htmlFile.getAbsolutePath(), e);
            System.exit(1);
            return null;
        }
    }

    private static Element findElementById(File htmlFile, String targetElementId) {
        try {
            Document doc = Jsoup.parse(
                    htmlFile,
                    CHARSET_NAME,
                    htmlFile.getAbsolutePath());
            Element element = doc.getElementById(targetElementId);
            if (Objects.isNull(element)) {
                LOGGER.info("current element with id {} not found in original file {}", targetElementId);
                System.exit(1);
            }
            return element;

        } catch (IOException e) {
            LOGGER.error("Error reading [{}] file", htmlFile.getAbsolutePath(), e);
            System.exit(1);
            return null;
        }
    }

    private static String getElementsAttr(Element element) {
        Optional<Node> elementValue = element.childNodes().stream().findFirst();
        return Stream.of(element).map(button ->
                button.attributes().asList().stream()
                        .map(attr -> attr.getKey() + " = " + attr.getValue())
                        .collect(Collectors.joining(", "))


        ).collect(Collectors.joining())
                .concat(", tagName = ".concat(element.tagName()))
                .concat(", value = ".concat(elementValue.isPresent() ? elementValue.get().toString() : ""));

    }

    private static String findArgument(String attr, String[] args) {
        Optional<String> arg = Arrays.stream(args).filter(strings -> strings.startsWith(attr.concat("=")))
                .findFirst();
        if (!arg.isPresent()) {
            LOGGER.info("argument {} not found", attr);
            System.exit(1);
        }
        String argsString = attr.concat("=");
        return arg.get().replaceAll(argsString, "");
    }

    private static void showDiff(Element originElement, Element sampleElement) {
        Map<String, String> origin = getElementAttributes(originElement);
        Map<String, String> sample = getElementAttributes(sampleElement);

        LOGGER.info("----Show diff----");

        origin.forEach((key, value) -> {
            String sampleValue = sample.get(key);
            if (Objects.nonNull(sampleValue) && !sampleValue.isEmpty()) {
                if (!sampleValue.equalsIgnoreCase(value))
                    LOGGER.info("origin attribute={}\r\n old value was={} \r\n new value={}", key, value, sampleValue);
            } else {
                LOGGER.info("origin attribute={} \r\n old value was={} \r\n new value={}", key, value, "");
            }
        });
        sample.forEach((key, value) -> {
            String newValues = origin.get(key);
            if (Objects.isNull(newValues))
                LOGGER.info("new attributes={} \r\n old value was={} \r\n new value={}", key, "", value);
        });
    }

    private static Map<String, String> getElementAttributes(Element element) {
        if (Objects.isNull(element))
            return new HashMap<>();
        Optional<Node> elementValue = element.childNodes().stream().findFirst();
        Map<String, String> response = element.attributes().asList()
                .stream()
                .collect(Collectors.toMap(Attribute::getKey, Attribute::getValue));
        response.put("tagName", element.tagName());
        response.put("value", elementValue.isPresent() ? elementValue.get().toString() : "");
        return response;


    }

}